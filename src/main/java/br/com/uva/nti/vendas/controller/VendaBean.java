package br.com.uva.nti.vendas.controller;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import br.com.uva.nti.vendas.dao.CategoriaDao;
import br.com.uva.nti.vendas.dao.ProdutoDao;
import br.com.uva.nti.vendas.dao.VendaDao;
import br.com.uva.nti.vendas.dao.VendaProdutoDao;
import br.com.uva.nti.vendas.entity.CategoriaProduto;
import br.com.uva.nti.vendas.entity.Produto;
import br.com.uva.nti.vendas.entity.Venda;
import br.com.uva.nti.vendas.entity.VendaProduto;

@SessionScoped
@ManagedBean(name="vendaBean")
public class VendaBean {

	private Venda venda;
	
	private VendaDao vendaDao;
	
	private VendaProdutoDao vendaProdutoDao;
	
	private ProdutoDao produtoDao;
	
	private Integer idProdutoAdicionar;

	private Integer idVendaSelecionada;

	private Integer quantidadeItem;
	
	private List<SelectItem> produtosSelect;
	
	private List<VendaProduto> listaVendaProduto;
	
	private Double total = 0.0;
	
	private Double totalComDesconto = 0.0;
 	
	@PostConstruct
    public void init() {
		vendaDao = new VendaDao();
		produtoDao = new ProdutoDao();
		vendaProdutoDao = new VendaProdutoDao();
		
		if(venda == null) {
			venda = new Venda();			
		}
	}
	
	public String persistir() {
		
		venda.setValorTotalSemDesconto(total);
		venda.setValorTotalComDesconto(totalComDesconto);
		
		for(VendaProduto vendaProduto : listaVendaProduto) {
			vendaProduto.setVenda(venda);
			Integer estoque = vendaProduto.getProduto().getQuantidadeEstoque();

			Integer novoEstoque = estoque - vendaProduto.getQuantidade();
			
			vendaProduto.getProduto().setQuantidadeEstoque(novoEstoque);
			
			produtoDao.save(vendaProduto.getProduto());
		}
		
		venda.setVendaProdutos(listaVendaProduto);
		
		vendaDao.save(venda);
		
		FacesContext.getCurrentInstance().addMessage(
				null, new FacesMessage("Venda salva com sucesso!", 
						"Venda salva com sucesso!"));
		
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getFlash().setKeepMessages(true);
		
		return "/logado/vendas.xhtml?faces-redirect=true";
	}
	
	public void excluir() {
		Venda venda = vendaDao.get(idVendaSelecionada);
		
		for(VendaProduto vendaProduto : venda.getVendaProdutos()) {
			Integer quantidadeEstoque = vendaProduto.getProduto().getQuantidadeEstoque();
			
			quantidadeEstoque += vendaProduto.getQuantidade();
			
			vendaProduto.getProduto().setQuantidadeEstoque(quantidadeEstoque);
			
			produtoDao.save(vendaProduto.getProduto());
			
			vendaProdutoDao.removeById(vendaProduto.getId());
		}
		
		vendaDao.removeById(venda.getId());
		
		FacesContext.getCurrentInstance().addMessage(
				null, new FacesMessage("Venda excluída com sucesso!", 
						"Venda excluída com sucesso!"));
		
		FacesContext.getCurrentInstance().addMessage(
				null, new FacesMessage("Estoque dos produtos atualizados!", 
						"Estoque dos produtos atualizados!"));
	}
	
	public Integer getIdVendaSelecionada() {
		return idVendaSelecionada;
	}

	public void setIdVendaSelecionada(Integer idVendaSelecionada) {
		this.idVendaSelecionada = idVendaSelecionada;
	}

	public Double getTotalComDesconto() {
		return totalComDesconto;
	}

	public void setTotalComDesconto(Double totalComDesconto) {
		this.totalComDesconto = totalComDesconto;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public List<VendaProduto> getListaVendaProduto() {
		return listaVendaProduto;
	}

	public void setListaVendaProduto(List<VendaProduto> listaVendaProduto) {
		this.listaVendaProduto = listaVendaProduto;
	}

	public void adicionarProduto() {
		
		if(listaVendaProduto == null) {
			listaVendaProduto = new ArrayList<VendaProduto>();			
		}

		Produto produto = produtoDao.get(idProdutoAdicionar);
		
		if(produto.getQuantidadeEstoque() > quantidadeItem) {

			VendaProduto vendaProduto = new VendaProduto();
			
			vendaProduto.setProduto(produto);
			
			vendaProduto.setQuantidade(quantidadeItem);
			
			vendaProduto.setPrecoVenda(produto.getPrecoVenda());
			
			total += produto.getPrecoVenda() * quantidadeItem;
			
			totalComDesconto = total;
			
			calcularDesconto();
			
			listaVendaProduto.add(vendaProduto);
			
			idProdutoAdicionar = null;
			
			quantidadeItem = null;
			
		} else {
			FacesContext.getCurrentInstance().addMessage(
		              null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
		            		  "Estoque insuficiente", "Estoque insuficiente"));
		}		
	}
	
	public void calcularDesconto() {
		if(total > 0 && venda.getDescontoPercentual() != null) {
			Double desconto = total * (venda.getDescontoPercentual() / 100);
			totalComDesconto = total - desconto;
		}
	}
	
	public Integer getQuantidadeItem() {
		return quantidadeItem;
	}

	public void setQuantidadeItem(Integer quantidadeItem) {
		this.quantidadeItem = quantidadeItem;
	}

	public Integer getIdProdutoAdicionar() {
		return idProdutoAdicionar;
	}

	public void setIdProdutoAdicionar(Integer idProdutoAdicionar) {
		this.idProdutoAdicionar = idProdutoAdicionar;
	}

	public List<SelectItem> getProdutosSelect() {
		if(produtosSelect == null) {
			 List<SelectItem> itens = new ArrayList<>();
			 for(Produto produto : produtoDao.todos()) {
				 itens.add(new SelectItem(produto.getId(), produto.getNome()));
			 }
			 
			 produtosSelect = itens;
		}

		return produtosSelect;
	}
	
	public String detalhes() {
		
		venda = vendaDao.get(idVendaSelecionada);

		return "/logado/vendaDetalhe.xhtml?faces-redirect=true";
	}
	
	public String cadastrar() {
		venda = new Venda();
		total = 0.0;
		totalComDesconto = 0.0;
		listaVendaProduto = null;
		idProdutoAdicionar = null;
		quantidadeItem = null;
		return "/logado/vendaForm.xhtml?faces-redirect=true";
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public List<Venda> getVendas() {
		return vendaDao.todos();
	}
}
