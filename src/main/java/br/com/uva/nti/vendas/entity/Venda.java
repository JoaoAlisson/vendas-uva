package br.com.uva.nti.vendas.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "venda")
public class Venda implements Serializable {
	
	static final long serialVersionUID = -8780407253943723401L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "cliente_nome", nullable = false)
    private String clienteNome;
	
	@Column(name = "cliente_cpf", length = 15, nullable = false)
    private String clienteCpf;
	
	@Column(name = "cliente_telefone", nullable = false)
    private String clienteTelefone;
    
	@Column(name = "cliente_email", nullable = false)
    private String clienteEmail;
	
	@Column(name = "desconto_percentual")
    private Double descontoPercentual;
	
	@Column(name = "valor_total_com_desconto", nullable = false)
    private Double valorTotalComDesconto;
	
	@Column(name = "valor_total_sem_desconto", nullable = false)
    private Double valorTotalSemDesconto;
	
	private Date data;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_venda")
	private List<VendaProduto> vendaProdutos = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClienteNome() {
		return clienteNome;
	}

	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}

	public String getClienteCpf() {
		return clienteCpf;
	}

	public void setClienteCpf(String clienteCpf) {
		this.clienteCpf = clienteCpf;
	}

	public String getClienteTelefone() {
		return clienteTelefone;
	}

	public void setClienteTelefone(String clienteTelefone) {
		this.clienteTelefone = clienteTelefone;
	}

	public String getClienteEmail() {
		return clienteEmail;
	}

	public void setClienteEmail(String clienteEmail) {
		this.clienteEmail = clienteEmail;
	}

	public Double getDescontoPercentual() {
		return descontoPercentual;
	}

	public void setDescontoPercentual(Double descontoPercentual) {
		this.descontoPercentual = descontoPercentual;
	}

	public Double getValorTotalComDesconto() {
		return valorTotalComDesconto;
	}

	public void setValorTotalComDesconto(Double valorTotalComDesconto) {
		this.valorTotalComDesconto = valorTotalComDesconto;
	}

	public Double getValorTotalSemDesconto() {
		return valorTotalSemDesconto;
	}

	public void setValorTotalSemDesconto(Double valorTotalSemDesconto) {
		this.valorTotalSemDesconto = valorTotalSemDesconto;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<VendaProduto> getVendaProdutos() {
		return vendaProdutos;
	}

	public void setVendaProdutos(List<VendaProduto> vendaProdutos) {
		this.vendaProdutos = vendaProdutos;
	}
}
