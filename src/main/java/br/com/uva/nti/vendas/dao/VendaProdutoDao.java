package br.com.uva.nti.vendas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.com.uva.nti.vendas.entity.Produto;
import br.com.uva.nti.vendas.entity.VendaProduto;
import br.com.uva.nti.vendas.util.PersistenceUtil;

public class VendaProdutoDao {

	public List<VendaProduto> todos() {

		EntityManager em = PersistenceUtil.getEntityManager();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<VendaProduto> cq = cb.createQuery(VendaProduto.class);
		
		cq.from(VendaProduto.class);		

	    return em.createQuery(cq).getResultList();
	}
	
	public VendaProduto get(Integer id) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();

        return entityManager.getReference(VendaProduto.class, id);
	}
	
	public Boolean save(VendaProduto vendaProduto) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();		
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		VendaProduto vendaProdutoBase = this.get(vendaProduto.getId());

		if(vendaProdutoBase != null) {
			VendaProduto produtoManaged = entityManager.merge(vendaProduto);
			entityManager.persist(produtoManaged);
		} else {
			entityManager.persist(vendaProduto);			
		}
		
		entityTransaction.commit();
		entityManager.close();

		PersistenceUtil.close();
		
		return true;
	}
	
	public Boolean removeById(Integer id) {
				
		EntityManager entityManager = PersistenceUtil.getEntityManager();
		
		VendaProduto vendaProduto = entityManager.getReference(VendaProduto.class, id);;
		
		EntityTransaction transaction = entityManager.getTransaction();

		transaction.begin();
		
		entityManager.remove(vendaProduto);
		  
		entityManager.getTransaction().commit();
		
		PersistenceUtil.close();
		
		return true;
	}
}
