# Sistema de Vendas
Aplicação desenvolvida para processo seletivo à vada de Analista de Sistema.    
Vaga publicada pelo Núcleo de Tecnologia da Informação da Universidade Estadual Vale do Acaraú.

## Autor
João Alisson

## Tecnologias do utilizadas

1. Java - 8
2. JSF - 2.2
3. Tomcat - 8
4. Hibernate - 5.4.1
5. Primefaces - 5.3
6. Postgres - 9.6

## Configurações para rodar o projeto

1. Clone o projeto:
```bash
git clone https://gitlab.com/JoaoAlisson/vendas-uva.git
```
2. Abrir o projeto na sua IDE (prefêncialmente eclipse)
3. Inserir as configurações do banco de dados do arquivo: **/src/main/resources/META-INF/persistence.xml**
4. Informar a pasta para salvar as imagens na classe: **src.com.uva.nti.vendas.controller.ProdutoBean.java** atributo **pastaImagens**
5. No eclipse: botão direito encima do projeto e selecionar: **Maven > Update Project**, depois confirmar.
    (Em outra IDE, seguir os passos para o Maven baixar as dependências)
6. Rodar sql do arquivo **base.sql** na base de dados: [base.sql](https://gitlab.com/JoaoAlisson/vendas-uva/-/snippets/2011197)
7. O projeto estará pronto para rodar em um servidor **tomcat 8**
8. Possível link para o sistema: **http://localhost:8080/vendas**
9. Usuário para teste   
    Email: **teste@uvanet.br** | Senha: **teste**

## DER da base
![baseDER](https://gitlab.com/JoaoAlisson/vendas-uva/-/wikis/uploads/96c8364233c19f56343d8b39909c2901/baseDER.png)

## Vídeo 1
[video1.mp4](https://drive.google.com/file/d/139MHnlHj6y7hgOnmnKKaEgDuCsF2iys-/view?usp=sharing)

## Vídeo 2
[video2.mp4](https://drive.google.com/file/d/1jU3TFTUbgixQdwCRfIkhPqYOrlLh5m3E/view?usp=sharing)

## Vídeo 3
[video3.mp4](https://drive.google.com/file/d/1LZu82zwR4HXKmB96t42Ro-QUESqPfHo-/view?usp=sharing)
