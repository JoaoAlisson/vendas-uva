package br.com.uva.nti.vendas.util;

import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Test;

public class UtilTest {

	@Test
	public void testValidacaoCpf() {
		
		Assert.assertTrue(Util.cpfValido("040.334.303-85"));
		
		Assert.assertFalse(Util.cpfValido("111.222.333-44"));
		
		Assert.assertFalse(Util.cpfValido(""));
	}
	
	@Test
	public void testGeracaoMd5() throws NoSuchAlgorithmException {
		
		Assert.assertEquals(Util.md5("123"), "202dcb962ac59075b964b07152d234b70");
		
		Assert.assertNotEquals(Util.md5("54321"), "202cb962ac59075b964b07152d234b70");		
	}
}
