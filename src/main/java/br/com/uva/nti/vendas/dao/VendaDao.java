package br.com.uva.nti.vendas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.uva.nti.vendas.entity.Produto;
import br.com.uva.nti.vendas.entity.Venda;
import br.com.uva.nti.vendas.entity.VendaProduto;
import br.com.uva.nti.vendas.util.PersistenceUtil;

public class VendaDao {

	public List<Venda> todos() {

		EntityManager entityManager = PersistenceUtil.getEntityManager();

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Venda> cq = cb.createQuery(Venda.class);
		
		Root<Venda> from = cq.from(Venda.class);
		
		cq.select(from);

	    cq.orderBy(cb.desc(from.get("id")));
	    
	    return entityManager.createQuery(cq).getResultList();
	}
	
	public Boolean save(Venda venda) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();		
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		Venda vendaBase = this.get(venda.getId());
		
		if(vendaBase != null) {
			Venda vendaManaged = entityManager.merge(venda);
			entityManager.persist(vendaManaged);
		} else {
			entityManager.persist(venda);
		}
		
		entityManager.flush();
		
		entityTransaction.commit();
		
		entityManager.close();

		PersistenceUtil.close();
		
		return true;
	}
	
	public Venda get(Integer id) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();

        return entityManager.getReference(Venda.class, id);
	}
	
	public Boolean removeById(Integer id) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();
		
		Venda venda = entityManager.getReference(Venda.class, id);;
		
		EntityTransaction transaction = entityManager.getTransaction();

		transaction.begin();
		
		entityManager.remove(venda);
		  
		entityManager.getTransaction().commit();
		
		PersistenceUtil.close();
		
		return true;
	}
}
