package br.com.uva.nti.vendas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.com.uva.nti.vendas.entity.Usuario;
import br.com.uva.nti.vendas.entity.Venda;
import br.com.uva.nti.vendas.util.PersistenceUtil;

public class UsuarioDao {

	public Usuario getByEmailSenha(String email, String senhaMd5) {

		EntityManager entityManager = PersistenceUtil.getEntityManager();
		
		Query query = entityManager.createQuery(
				"SELECT u FROM Usuario u WHERE u.email = :email and u.senha = :senha"
		);
		
        query.setParameter("email", email);
        query.setParameter("senha", senhaMd5);
	      
        List<Usuario> usuarios = query.getResultList();		
		
		Usuario usuarioRetorno = null;
		
		for(Usuario usuario : usuarios) {
			usuarioRetorno = usuario;
		}
		
		return usuarioRetorno;
	}

}
