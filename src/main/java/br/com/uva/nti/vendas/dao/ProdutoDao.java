package br.com.uva.nti.vendas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.uva.nti.vendas.entity.Produto;
import br.com.uva.nti.vendas.util.PersistenceUtil;

public class ProdutoDao {

	public List<Produto> todos() {

		EntityManager entityManager = PersistenceUtil.getEntityManager();

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Produto> cq = cb.createQuery(Produto.class);
		
		Root<Produto> from = cq.from(Produto.class);
		
		cq.select(from);

	    cq.orderBy(cb.desc(from.get("id")));
	    
	    return entityManager.createQuery(cq).getResultList();
	}
	
	public Produto get(Integer id) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();

        return entityManager.getReference(Produto.class, id);
	}
	
	public Boolean save(Produto produto) {
		
		EntityManager entityManager = PersistenceUtil.getEntityManager();		
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		Produto produtoBase = this.get(produto.getId());

		if(produtoBase != null) {
			Produto produtoManaged = entityManager.merge(produto);
			entityManager.persist(produtoManaged);
		} else {
			entityManager.persist(produto);			
		}
		
		entityTransaction.commit();
		entityManager.close();

		PersistenceUtil.close();
		
		return true;
	}
	
	public Boolean removeById(Integer id) {
				
		EntityManager entityManager = PersistenceUtil.getEntityManager();
		
		Produto produto = entityManager.getReference(Produto.class, id);;
		
		EntityTransaction transaction = entityManager.getTransaction();

		transaction.begin();
		
		entityManager.remove(produto);
		  
		entityManager.getTransaction().commit();
		
		PersistenceUtil.close();
		
		return true;
	}
}
