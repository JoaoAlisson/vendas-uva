package br.com.uva.nti.vendas.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.UploadedFile;

import br.com.uva.nti.vendas.dao.CategoriaDao;
import br.com.uva.nti.vendas.dao.ProdutoDao;
import br.com.uva.nti.vendas.entity.CategoriaProduto;
import br.com.uva.nti.vendas.entity.Produto;
import br.com.uva.nti.vendas.util.Util;

@SessionScoped
@ManagedBean
public class ProdutoBean implements Serializable {
	
	static final long serialVersionUID = -8780407253943723401L;
	
	private static String pastaImagens = "/home/alisson/imagens";
	
    private ProdutoDao produtoDao;
    
    private Produto produto;
    
    private Integer idProdutoSelecionado;
    
    private List<SelectItem> categorias;
    
    private CategoriaDao categoriaDao;
    
    private CategoriaProduto categoriaProduto;
    
    private UploadedFile arquivoImagem;
    
	@PostConstruct
    public void init() {
		produto = new Produto();
		produto.setCategoriaProduto(new CategoriaProduto());
		produtoDao = new ProdutoDao();
		categoriaDao = new CategoriaDao();
	}
	
	public static String getPastaImagens()
	{
		return pastaImagens;
	}
	
	public String getCaminhoImagem(String imagem) {
		return "/images/dynamic/?file=" + imagem;
	}

	public UploadedFile getArquivoImagem() {
		return arquivoImagem;
	}

	public void setArquivoImagem(UploadedFile arquivoImagem) {
		this.arquivoImagem = arquivoImagem;
	}

	public List<SelectItem> getCategorias() {
		if(categorias == null) {
			List<SelectItem> itens = new ArrayList<>();
			 for(CategoriaProduto categoriaProduto : categoriaDao.todos()) {
				 itens.add(new SelectItem(categoriaProduto.getId(), categoriaProduto.getNome()));
			 }
			 
			 categorias = itens;
		}

		return categorias;
	}

	public CategoriaProduto getCategoriaProduto() {
		return categoriaProduto;
	}

	public void setCategoriaProduto(CategoriaProduto categoriaProduto) {
		this.categoriaProduto = categoriaProduto;
	}

	public void setCategorias(List<SelectItem> categorias) {
		this.categorias = categorias;
	}
	
	public String cadastrar() {
		produto = new Produto();
		produto.setCategoriaProduto(new CategoriaProduto());
		return "/logado/produtoForm.xhtml?faces-redirect=true";
	}
	
	public String persistir() throws NoSuchAlgorithmException {
		
		Boolean imagemSubmetidaComSucesso = uploadImagem();

		if(imagemSubmetidaComSucesso) {
			produtoDao.save(produto);
			
			FacesContext.getCurrentInstance().addMessage(
					null, new FacesMessage("Produto salvo com sucesso!", 
							"Produto salvo com sucesso!"));	
			
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			
			return "/logado/produtos.xhtml?faces-redirect=true";
		}
		
		return null;
	}
	
	public void excluir() {
		produtoDao.removeById(idProdutoSelecionado);

		FacesContext.getCurrentInstance().addMessage(
				null, new FacesMessage("Produto excluído com sucesso!", 
						"Produto excluído com sucesso!"));
	}
	
	private Boolean uploadImagem() throws NoSuchAlgorithmException {
		try {
			String extensao = FilenameUtils.getExtension(arquivoImagem.getFileName());
			String nomeArquivoMd5 = Util.md5(arquivoImagem.getFileName()) + "." + extensao;
			
			produto.setImagem(nomeArquivoMd5);

		    File file = new File(pastaImagens, nomeArquivoMd5);
		 
		    OutputStream out = new FileOutputStream(file);
		    out.write(arquivoImagem.getContents());
		    out.close();

		    return true;
		  } catch(IOException e) {
		    FacesContext.getCurrentInstance().addMessage(
		              null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
		            		  "Erro: " + e.getMessage(), e.getMessage()));
		    
		    return false;
		  }
	}
	
	public String editar() {
		produto = produtoDao.get(idProdutoSelecionado);
		return "/logado/produtoForm.xhtml?faces-redirect=true";
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<Produto> getProdutos() {
		return produtoDao.todos();
	}

	public void criarProduto() {
		
	}

	public Integer getIdProdutoSelecionado() {
		return idProdutoSelecionado;
	}

	public void setIdProdutoSelecionado(Integer idProdutoSelecionado) {
		this.idProdutoSelecionado = idProdutoSelecionado;
	}
}
