package br.com.uva.nti.vendas.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceUtil {
	
	private static EntityManagerFactory entityManagerFactory;

	public static EntityManagerFactory getEntityManagerFactory() {
		
		if(entityManagerFactory == null) {
			entityManagerFactory = Persistence.createEntityManagerFactory("vendas");
		}
		
		return entityManagerFactory;
	}
	
	public static EntityManager getEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}
	
	public static void close() {
		getEntityManagerFactory().close();
		entityManagerFactory = null;
	}
}
