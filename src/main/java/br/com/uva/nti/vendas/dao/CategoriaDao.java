package br.com.uva.nti.vendas.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import br.com.uva.nti.vendas.entity.CategoriaProduto;
import br.com.uva.nti.vendas.util.PersistenceUtil;

public class CategoriaDao {

	public List<CategoriaProduto> todos() {

		EntityManager entityManager = PersistenceUtil.getEntityManager();

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CategoriaProduto> cq = cb.createQuery(CategoriaProduto.class);
		
		cq.from(CategoriaProduto.class);

	    return entityManager.createQuery(cq).getResultList();
	}
}
