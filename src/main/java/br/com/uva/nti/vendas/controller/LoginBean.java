package br.com.uva.nti.vendas.controller;

import java.security.NoSuchAlgorithmException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.com.uva.nti.vendas.dao.UsuarioDao;
import br.com.uva.nti.vendas.entity.Usuario;
import br.com.uva.nti.vendas.util.SessionContext;
import br.com.uva.nti.vendas.util.Util;

@ManagedBean
@SessionScoped
public class LoginBean {

	private String email;
    private String senha;
    private Usuario usuario;
    
    public String logar() throws NoSuchAlgorithmException {
    
    	UsuarioDao usuarioDao = new UsuarioDao();

    	String senhaMd5 = Util.md5(senha);
    	
    	usuario = usuarioDao.getByEmailSenha(email, senhaMd5);

    	if(usuario == null) {
			FacesContext.getCurrentInstance().addMessage(
		              null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
		            		  "Usuário não encontrado", 
		            		  "Verifique os dados e tente novamente"));
    		return "";
    	} else {
    		FacesContext.getCurrentInstance().addMessage(
    				null, new FacesMessage("Login realizado com sucesso!", 
    						"Login realizado com sucesso!"));

    		FacesContext context = FacesContext.getCurrentInstance();
    		context.getExternalContext().getFlash().setKeepMessages(true);

    		SessionContext.getInstance().setAttribute("usuarioLogado", usuario);
    		
    		return "/logado/produtos.xhtml?faces-redirect=true";
    	}
    }
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Usuario getUsuario() {
		return usuario;
	}   
}
